﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebUI.Models
{
    public class SurveyPostParams
    {
        /// <summary>
        /// Пол опрошенного.
        /// </summary>
        [Required]
        public bool? Sex { get; set; }

        /// <summary>
        /// Есть ли холодильник.
        /// </summary>
        public bool HasFridge { get; set; }

        /// <summary>
        /// Есть ли телевизор.
        /// </summary>
        public bool HasTV { get; set; }

        /// <summary>
        /// Есть ли компьютер.
        /// </summary>
        public bool HasPC { get; set; }
    }
}