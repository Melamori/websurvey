﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebUI.Models;
using System.Data.SqlClient;
using System.Configuration;

namespace WebUI.Controllers
{
    /// <summary>
    /// Основной контроллер (в данном приложении — единственный).
    /// </summary>
    public class SurveyController : Controller
    {
        /// <summary>
        /// Метод по умолчанию (пустая страница анкеты).
        /// </summary>
        /// <returns>Страница с пустой анкетой</returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Метод для проверки отправляемых на сервер данных из анкеты.
        /// </summary>
        /// <returns>Результат проверки заполнения анкеты.</returns>
        public ActionResult FormHandler(SurveyPostParams parameters)
        {
            bool sexError = false;
            bool thingsError = false;
            if (!ModelState.IsValid)
            {
                sexError = true;
            }

            if (!parameters.HasFridge && !parameters.HasTV && !parameters.HasPC)
            {
                thingsError = true;
            }

            if (sexError || thingsError)
            {
                return Json(new { success = false, sexError, thingsError});
            }

            var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SurveyDBConnectionString"].ConnectionString);
            var cmd = new SqlCommand
            {
                CommandType = System.Data.CommandType.Text,
                CommandText = "INSERT INTO Answers (Sex, HasFridge, HasTV, HasPC) Values (@sex, @hasFridge, @hasTV, @hasPC)"
            };

            cmd.Parameters.AddWithValue("@sex", parameters.Sex);
            cmd.Parameters.AddWithValue("@hasFridge", parameters.HasFridge);
            cmd.Parameters.AddWithValue("@hasTV", parameters.HasTV);
            cmd.Parameters.AddWithValue("@hasPC", parameters.HasPC);
            cmd.Connection = conn;

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            return Json(new { success = true });
        }

        /// <summary>
        /// Метод для отображения страницы статистики.
        /// </summary>
        /// <returns>Страница статистики.</returns>
        public ActionResult Statistics()
        {
            var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SurveyDBConnectionString"].ConnectionString);
            var dataAdapter = new SqlDataAdapter("SELECT * FROM Answers", conn);
            var dataSet = new Answers();
            conn.Open();
            dataAdapter.Fill(dataSet, "Answers");
            conn.Close();

            int count = dataSet.Tables["Answers"].Rows.Count;
            ViewBag.MalePercentage = Math.Round((decimal)dataSet.Tables["Answers"].Select("Sex = true").Count() / count * 100);
            ViewBag.FemalePercentage = 100 - ViewBag.MalePercentage;
            ViewBag.HasFridgePercentage = Math.Round((decimal)dataSet.Tables["Answers"].Select("HasFridge = true").Count() / count * 100);
            ViewBag.HasTVPercentage = Math.Round((decimal)dataSet.Tables["Answers"].Select("HasTV = true").Count() / count * 100);
            ViewBag.HasPCPercentage = Math.Round((decimal)dataSet.Tables["Answers"].Select("HasPC = true").Count() / count * 100);

            return View();
        }
	}
}